#!/bin/bash
# Función para solicitar y validar inputs
request_input() {
  local input_name="$1"
  local input_value=""
  
  while [ -z "$input_value" ]; do
    read -p "$input_name: " input_value
    if [ -z "$input_value" ]; then
      echo "$input_name es obligatorio. Por favor, intente de nuevo."
    fi
  done

  echo "$input_value"
}

ARTIFACTORY_USER=$(request_input "Ingrese el usuario de Artifactory")
ARTIFACTORY_API_KEY=$(request_input "Ingrese la API Key de Artifactory")

echo "Instalando bower..."
npm install -g bower
echo "Instalando bower-art-resolver..."
npm install -g bower-art-resolver


echo "{ \"registry\": \"https://$ARTIFACTORY_USER:$ARTIFACTORY_API_KEY@artifactory.globaldevtools.bbva.com:443/artifactory/api/bower/gl-cells-bower-virtual\", \"resolvers\": [ \"bower-art-resolver\" ] }" > ~/.bowerrc

echo "Bower instalado correctamente."





#!/bin/bash
# Función para solicitar y validar inputs
request_input() {
  local input_name="$1"
  local input_value=""
  
  while [ -z "$input_value" ]; do
    read -p "$input_name: " input_value
    if [ -z "$input_value" ]; then
      echo "$input_name es obligatorio. Por favor, intente de nuevo."
    fi
  done

  echo "$input_value"
}

IS_EXE_SSH_KEY=$(request_input "Desea generar las llaves con SSH Keygen (Si=s, No=n)")
if [ "$IS_EXE_SSH_KEY" == "s" ]; then
  ssh-keygen
  echo -e "Copia la llave publica y configurala en el SSHKeys de Bitbucket"
  echo -e "$(cat ~/.ssh/id_rsa.pub)"
  echo -e "Presione enter para continuar..."
  read result_continue 
fi


# Solicitar inputs del usuario
ARTIFACTORY_USER=$(request_input "Ingrese el usuario de Artifactory")
ARTIFACTORY_API_KEY=$(request_input "Ingrese la API Key de Artifactory")
NODE_VERSION=$(request_input "Ingrese la versión de Node.js que desea instalar")

# Función para mostrar las versiones de Node y npm
show_node_versions() {
  NODE_VERSION_INSTALLED=$(node -v)
  NPM_VERSION_INSTALLED=$(npm -v)
  CELLS_VERSION_INSTALLED=$(cells -v)
  CELLSGEN_VERSION_INSTALLED=$(cellsgen -V)
  echo "Node.js instalado: $NODE_VERSION_INSTALLED"
  echo "npm instalado: $NPM_VERSION_INSTALLED"
  echo "Cells instalado: $CELLS_VERSION_INSTALLED"
  echo "Cells Gen instalado: $CELLSGEN_VERSION_INSTALLED"
}

# Función para instalar Yarn y Cells CLI
install_tools() {

  read -p "Ingrese la versión de Cells CLI que desea instalar (deje en blanco para la última versión): " CELLS_VERSION
  if [ -z "$CELLS_VERSION" ]; then
    echo "Instalando @cells/cells-cli..."
    npm install -g @cells/cells-cli
  else
    echo "Instalando @cells/cells-cli@$CELLS_VERSION..."
    npm install -g @cells/cells-cli@$CELLS_VERSION
  fi

  npm install -g yarn

  npm install -g cells-gen-cli


}

#Validar si Node.js ya está instalado
if command -v node &> /dev/null && command -v npm &> /dev/null; then
  echo "Node.js y npm ya están instalados."
  install_tools
  show_node_versions
  exit 0
fi

# Detectar sistema operativo y arquitectura
OS=$(uname -s)
ARCH=$(uname -m)

# Definir la URL de descarga según el sistema operativo y arquitectura
if [ "$OS" == "Linux" ]; then
  if [ "$ARCH" == "x86_64" ]; then
    NODE_URL="https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz"
  elif [ "$ARCH" == "aarch64" ]; then
    NODE_URL="https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-arm64.tar.xz"
  else
    echo "Arquitectura no soportada: $ARCH"
    exit 1
  fi
elif [ "$OS" == "Darwin" ]; then
  if [ "$ARCH" == "x86_64" ]; then
    NODE_URL="https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-darwin-x64.tar.xz"
  elif [ "$ARCH" == "arm64" ]; then
    NODE_URL="https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-darwin-arm64.tar.xz"
  else
    echo "Arquitectura no soportada: $ARCH"
    exit 1
  fi
else
  echo "Sistema operativo no soportado: $OS"
  exit 1
fi

# Crear directorio de destino
NODE_DIR="$HOME/node_versions/node-v${NODE_VERSION}"
mkdir -p "$NODE_DIR"

# Descargar y extraer Node.js
echo "Descargando Node.js desde $NODE_URL..."
curl -o node.tar.xz "$NODE_URL"
tar -xJf node.tar.xz -C "$NODE_DIR" --strip-components=1
rm node.tar.xz

# Agregar Node.js al PATH
export PATH=$PATH:$NODE_DIR/bin >> ~/.bashrc
source ~/.bashrc
export PATH=$PATH:$NODE_DIR/bin >> ~/.profile
source ~/.profile


# Configure .npmrc
npm config set registry https://artifactory.globaldevtools.bbva.com:443/artifactory/api/npm/gl-bbva-npm-virtual
curl -u $ARTIFACTORY_USER:$ARTIFACTORY_API_KEY "https://artifactory.globaldevtools.bbva.com:443/artifactory/api/npm/auth" --insecure >> ~/.npmrc

# Resolve fix _auth .npmrc
npm config fix

# Instalar Yarn y Cells CLI y CellsGen
install_tools

echo "export PATH=\$PATH:$NODE_DIR/bin" >> ~/.bashrc
source ~/.bashrc

echo "export PATH=\$PATH:$NODE_DIR/bin" >> ~/.zshrc
source ~/.zshrc


# Mostrar versiones de Node.js y npm
echo "Node y cells instalados correctamente."
show_node_versions
echo -e "Cierre el terminal para visualizar los cambios"
echo -e "Presione enter para continuar..."
read result_continue
kill -9 $PPID



